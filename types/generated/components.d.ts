import type { Schema, Attribute } from '@strapi/strapi';

export interface CommonActionButton extends Schema.Component {
  collectionName: 'components_common_action_buttons';
  info: {
    displayName: 'ActionButton';
    description: '';
  };
  attributes: {
    text: Attribute.String & Attribute.Required;
    action: Attribute.Enumeration<
      [
        'open_composer',
        'open_uniswap_v2_liquidity',
        'open_onchain_transfer_modal',
        'scroll_to_top',
        'open_register_modal',
        'open_plus_upgrade_modal',
        'open_pro_upgrade_modal',
        'networks_team_checkout',
        'networks_community_checkout',
        'networks_enterprise_checkout',
        'networks_start_trial'
      ]
    >;
    navigationUrl: Attribute.String;
    dataRef: Attribute.String;
    solid: Attribute.Boolean;
  };
}

export interface ExplainerScreenContinueButton extends Schema.Component {
  collectionName: 'components_explainer_screen_continue_buttons';
  info: {
    displayName: 'Continue Button';
  };
  attributes: {
    text: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 2;
      }> &
      Attribute.DefaultTo<'Continue'>;
    dataRef: Attribute.String &
      Attribute.Required &
      Attribute.DefaultTo<'explainer-screen-continue-button'>;
  };
}

export interface ExplainerScreenSection extends Schema.Component {
  collectionName: 'components_explainer_screen_sections';
  info: {
    displayName: 'Section';
    description: '';
  };
  attributes: {
    icon: Attribute.String & Attribute.Required;
    title: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 1;
      }>;
    description: Attribute.RichText &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 1;
      }>;
  };
}

export interface FooterColumns extends Schema.Component {
  collectionName: 'components_footer_columns';
  info: {
    displayName: 'columns';
    description: '';
  };
  attributes: {
    title: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 2;
      }>;
    links: Attribute.Component<'footer.link', true> &
      Attribute.Required &
      Attribute.SetMinMax<
        {
          min: 1;
        },
        number
      >;
  };
}

export interface FooterLink extends Schema.Component {
  collectionName: 'components_footer_links';
  info: {
    displayName: 'Link';
  };
  attributes: {
    text: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 2;
      }>;
    url: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 1;
      }> &
      Attribute.DefaultTo<'/'>;
    dataRef: Attribute.String &
      Attribute.SetMinMaxLength<{
        minLength: 3;
      }> &
      Attribute.DefaultTo<'footer-link-default'>;
  };
}

export interface MarketingComponentsAsFeaturedIn extends Schema.Component {
  collectionName: 'components_marketing_components_as_featured_ins';
  info: {
    displayName: 'As Featured In';
    description: '';
  };
  attributes: {
    image: Attribute.Media & Attribute.Private;
  };
}

export interface MarketingComponentsHero extends Schema.Component {
  collectionName: 'components_marketing_components_heroes';
  info: {
    displayName: 'Hero';
    description: '';
  };
  attributes: {
    h1: Attribute.String & Attribute.Required;
    body: Attribute.RichText & Attribute.Required;
    ctaText: Attribute.String & Attribute.Required;
    heroBackground: Attribute.Media;
    heroStats: Attribute.Component<'marketing-components.stats-bar-stat', true>;
  };
}

export interface MarketingComponentsMarketingPageSection
  extends Schema.Component {
  collectionName: 'components_marketing_components_marketing_page_sections';
  info: {
    displayName: 'Marketing Page Section';
    description: '';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    body: Attribute.RichText & Attribute.Required;
    image: Attribute.Media & Attribute.Required;
    leftAligned: Attribute.Boolean &
      Attribute.Required &
      Attribute.DefaultTo<true>;
  };
}

export interface MarketingComponentsSectionTail extends Schema.Component {
  collectionName: 'components_marketing_components_section_tails';
  info: {
    displayName: 'Section Tail';
  };
  attributes: {
    h3: Attribute.String & Attribute.Required;
    ctaText: Attribute.String & Attribute.Required;
  };
}

export interface MarketingComponentsStatsBarStat extends Schema.Component {
  collectionName: 'components_marketing_components_stats_bar_stats';
  info: {
    displayName: 'Stats Bar Stat';
    description: '';
  };
  attributes: {
    number: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        maxLength: 3;
      }>;
    label: Attribute.String & Attribute.Required;
  };
}

export interface MetadataGeneralPageMetadata extends Schema.Component {
  collectionName: 'components_metadata_general_page_metadata';
  info: {
    displayName: 'GeneralMetadata';
    description: '';
  };
  attributes: {
    title: Attribute.String;
    description: Attribute.String;
    ogImage: Attribute.Media;
    canonicalUrl: Attribute.String;
    ogUrl: Attribute.String;
    ogType: Attribute.String;
    robots: Attribute.String;
    author: Attribute.String;
    ogAuthor: Attribute.String;
  };
}

export interface OnboardingV5ActionButton extends Schema.Component {
  collectionName: 'components_onboarding_v5_action_buttons';
  info: {
    displayName: 'Action Button';
    description: '';
  };
  attributes: {
    text: Attribute.String &
      Attribute.Required &
      Attribute.DefaultTo<'Continue'>;
    dataRef: Attribute.String & Attribute.DefaultTo<'onboarding-next-button'>;
  };
}

export interface OnboardingV5CarouselItem extends Schema.Component {
  collectionName: 'components_onboarding_v5_carousel_items';
  info: {
    displayName: 'Carousel Item';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    media: Attribute.Media & Attribute.Required;
  };
}

export interface OnboardingV5CompletionStep extends Schema.Component {
  collectionName: 'components_onboarding_v5_completion_steps';
  info: {
    displayName: 'Completion Step';
  };
  attributes: {
    message: Attribute.String & Attribute.Required;
    media: Attribute.Media;
  };
}

export interface OnboardingV5GroupSelectorStep extends Schema.Component {
  collectionName: 'components_onboarding_v5_group_selector_steps';
  info: {
    displayName: 'Group Selector';
    description: '';
  };
  attributes: {};
}

export interface OnboardingV5OnboardingStep extends Schema.Component {
  collectionName: 'components_onboarding_v5_onboarding_steps';
  info: {
    displayName: 'Onboarding Step';
    description: '';
  };
  attributes: {
    carousel: Attribute.Component<'onboarding-v5.carousel-item', true> &
      Attribute.Required;
    title: Attribute.String & Attribute.Required;
    description: Attribute.String & Attribute.Required;
    stepType: Attribute.Enumeration<
      [
        'verify_email',
        'tag_selector',
        'survey',
        'user_selector',
        'group_selector'
      ]
    > &
      Attribute.Required;
    stepKey: Attribute.String & Attribute.Required;
    verifyEmailForm: Attribute.Component<'onboarding-v5.verify-email-step'>;
    tagSelector: Attribute.Component<'onboarding-v5.tag-selector-step'>;
    radioSurveyQuestion: Attribute.String;
    radioSurvey: Attribute.Component<'onboarding-v5.radio-option', true> &
      Attribute.SetMinMax<
        {
          min: 2;
        },
        number
      >;
    userSelector: Attribute.Component<'onboarding-v5.user-selector-step'>;
    groupSelector: Attribute.Component<'onboarding-v5.group-selector-step'>;
    actionButton: Attribute.Component<'onboarding-v5.action-button'>;
    skipButton: Attribute.Component<'onboarding-v5.skip-button'>;
  };
}

export interface OnboardingV5RadioOption extends Schema.Component {
  collectionName: 'components_onboarding_v5_radio_option';
  info: {
    displayName: 'Survey Radio Option';
    description: '';
  };
  attributes: {
    optionTitle: Attribute.String & Attribute.Required;
    optionDescription: Attribute.String & Attribute.Required;
    optionKey: Attribute.String & Attribute.Required;
  };
}

export interface OnboardingV5SkipButton extends Schema.Component {
  collectionName: 'components_onboarding_v5_skip_buttons';
  info: {
    displayName: 'Skip Button';
  };
  attributes: {
    text: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 2;
      }> &
      Attribute.DefaultTo<'Skip'>;
    dataRef: Attribute.String;
  };
}

export interface OnboardingV5TagSelectorStep extends Schema.Component {
  collectionName: 'components_onboarding_v5_tag_selector_steps';
  info: {
    displayName: 'Tag Selector';
    description: '';
  };
  attributes: {
    customTagInputText: Attribute.String &
      Attribute.Required &
      Attribute.DefaultTo<'Didn\u2019t find what you were looking for? Add a custom tag'>;
  };
}

export interface OnboardingV5UserSelectorStep extends Schema.Component {
  collectionName: 'components_onboarding_v5_user_selector_steps';
  info: {
    displayName: 'User Selector';
    description: '';
  };
  attributes: {};
}

export interface OnboardingV5VerifyEmailStep extends Schema.Component {
  collectionName: 'components_onboarding_v5_verify_email_steps';
  info: {
    displayName: 'Verify Email Form';
    description: '';
  };
  attributes: {
    inputLabel: Attribute.String &
      Attribute.Required &
      Attribute.DefaultTo<'Verification code'>;
    inputPlaceholder: Attribute.String;
    resendCodeText: Attribute.String &
      Attribute.Required &
      Attribute.DefaultTo<'Didn\u2019t get a code? Check your spam folder, or {action}.'>;
    resendCodeActionText: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 2;
      }> &
      Attribute.DefaultTo<'resend code'>;
    changeEmailActionText: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 3;
      }> &
      Attribute.DefaultTo<'Change email address'>;
    changeEmailTitle: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 3;
      }> &
      Attribute.DefaultTo<'Change email address'>;
    changeEmailDescription: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 3;
      }> &
      Attribute.DefaultTo<'Mistyped your email? Change your email address below.'>;
    changeEmailInputLabel: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 3;
      }>;
    changeEmailInputPlaceholder: Attribute.String;
    changeEmailActionButton: Attribute.Component<'onboarding-v5.action-button'> &
      Attribute.Required;
  };
}

export interface ProductFooter extends Schema.Component {
  collectionName: 'components_product_footers';
  info: {
    displayName: 'Footer';
  };
  attributes: {
    actionButton: Attribute.Component<'common.action-button'>;
  };
}

export interface ProductHero extends Schema.Component {
  collectionName: 'components_product_heroes';
  info: {
    displayName: 'Hero';
  };
  attributes: {
    h1: Attribute.String & Attribute.Required;
    body: Attribute.RichText & Attribute.Required;
    image: Attribute.Media & Attribute.Required;
    showBackgroundEffects: Attribute.Boolean &
      Attribute.Required &
      Attribute.DefaultTo<true>;
  };
}

export interface ProductOther extends Schema.Component {
  collectionName: 'components_product_others';
  info: {
    displayName: 'Other';
  };
  attributes: {
    title: Attribute.String;
    column1Title: Attribute.String & Attribute.Required;
    column1Body: Attribute.RichText & Attribute.Required;
    column2Title: Attribute.String & Attribute.Required;
    column2Body: Attribute.RichText & Attribute.Required;
    column3Title: Attribute.String & Attribute.Required;
    column3Body: Attribute.RichText & Attribute.Required;
  };
}

export interface ProductSection extends Schema.Component {
  collectionName: 'components_product_sections';
  info: {
    displayName: 'Section';
  };
  attributes: {
    title: Attribute.String & Attribute.Required;
    body: Attribute.RichText & Attribute.Required;
    image: Attribute.Media;
    imageOverlay: Attribute.Media;
    actionButtons: Attribute.Component<'common.action-button', true>;
    leftAligned: Attribute.Boolean &
      Attribute.Required &
      Attribute.DefaultTo<true>;
    showBackgroundEffects: Attribute.Boolean &
      Attribute.Required &
      Attribute.DefaultTo<true>;
    showBodyBackground: Attribute.Boolean &
      Attribute.Required &
      Attribute.DefaultTo<true>;
  };
}

export interface TagsTags extends Schema.Component {
  collectionName: 'components_tags_tags';
  info: {
    displayName: 'Tags';
  };
  attributes: {
    tag: Attribute.String;
  };
}

export interface V2ProductActionButton extends Schema.Component {
  collectionName: 'components_v2-product_action_buttons';
  info: {
    displayName: 'ActionButton';
    description: '';
  };
  attributes: {
    text: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 2;
      }>;
    action: Attribute.Enumeration<
      [
        'open_composer',
        'open_uniswap_v2_liquidity',
        'open_onchain_transfer_modal',
        'scroll_to_top',
        'open_register_modal',
        'open_plus_upgrade_modal',
        'open_pro_upgrade_modal',
        'networks_team_checkout',
        'networks_community_checkout',
        'networks_enterprise_checkout',
        'networks_start_trial'
      ]
    >;
    navigationUrl: Attribute.String;
    dataRef: Attribute.String;
    solid: Attribute.Boolean;
    rounded: Attribute.Boolean;
    stripeProductKey: Attribute.String;
    trialUpgradeRequest: Attribute.Boolean & Attribute.DefaultTo<false>;
  };
}

export interface V2ProductBasicExplainer extends Schema.Component {
  collectionName: 'components_v2-product_basic_explainers';
  info: {
    displayName: 'Basic Explainer';
    description: '';
  };
  attributes: {
    title: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 3;
      }>;
    body: Attribute.RichText &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 3;
      }>;
    button: Attribute.Component<'v2-product.action-button'>;
  };
}

export interface V2ProductClosingCta extends Schema.Component {
  collectionName: 'components_v2-product_closing_ctas';
  info: {
    displayName: 'Closing CTA';
    description: '';
  };
  attributes: {
    title: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 2;
      }>;
    body: Attribute.RichText &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 2;
      }>;
    button: Attribute.Component<'v2-product.action-button'>;
    borderImage: Attribute.Media;
  };
}

export interface V2ProductFeatureHighlight extends Schema.Component {
  collectionName: 'components_v2-product_feature_highlights';
  info: {
    displayName: 'Feature Highlight';
    description: '';
  };
  attributes: {
    title: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 2;
      }>;
    backgroundColor: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 3;
      }> &
      Attribute.DefaultTo<'#000000'>;
    image: Attribute.Media & Attribute.Required;
    body: Attribute.RichText &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 3;
      }>;
    alignImage: Attribute.Enumeration<['left', 'right']> & Attribute.Required;
    button: Attribute.Component<'v2-product.action-button'>;
    colorScheme: Attribute.Enumeration<['dark', 'light']> &
      Attribute.Required &
      Attribute.DefaultTo<'dark'>;
    footnotes: Attribute.RichText;
  };
}

export interface V2ProductFeatureShowcaseItem extends Schema.Component {
  collectionName: 'components_v2-product_feature_showcase_items';
  info: {
    displayName: 'Feature Showcase Item';
    description: '';
  };
  attributes: {
    image: Attribute.Media & Attribute.Required;
    title: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 3;
      }>;
    body: Attribute.RichText &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 3;
      }>;
  };
}

export interface V2ProductFeatureShowcase extends Schema.Component {
  collectionName: 'components_v2-product_feature_showcases';
  info: {
    displayName: 'Feature Showcase';
    description: '';
  };
  attributes: {
    items: Attribute.Component<'v2-product.feature-showcase-item', true> &
      Attribute.Required &
      Attribute.SetMinMax<
        {
          min: 3;
          max: 3;
        },
        number
      >;
  };
}

export interface V2ProductFeatureTableHeader extends Schema.Component {
  collectionName: 'components_v2-product_feature_table_headers';
  info: {
    displayName: 'Feature Table Header';
    description: '';
  };
  attributes: {
    title: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 2;
      }>;
    button: Attribute.Component<'v2-product.action-button'> &
      Attribute.Required;
    priceStartingAt: Attribute.Boolean & Attribute.DefaultTo<false>;
    noPrice: Attribute.Boolean & Attribute.DefaultTo<false>;
  };
}

export interface V2ProductFeatureTable extends Schema.Component {
  collectionName: 'components_v2-product_feature_tables';
  info: {
    displayName: 'Feature Table';
    description: '';
  };
  attributes: {
    title: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 3;
      }>;
    subtitle: Attribute.String &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 3;
      }>;
    columns: Attribute.Relation<
      'v2-product.feature-table',
      'oneToMany',
      'api::feat-table-column.feat-table-column'
    >;
  };
}

export interface V2ProductHero extends Schema.Component {
  collectionName: 'components_v2-product_heroes';
  info: {
    displayName: 'Hero';
    description: '';
  };
  attributes: {
    text: Attribute.RichText &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 2;
      }>;
    buttons: Attribute.Component<'common.action-button', true>;
  };
}

export interface V2ProductImageCard extends Schema.Component {
  collectionName: 'components_v2_product_image_cards';
  info: {
    displayName: 'Image Card';
    description: '';
  };
  attributes: {
    image: Attribute.Media & Attribute.Required;
  };
}

export interface V2ProductPerk extends Schema.Component {
  collectionName: 'components_v2-product_perks';
  info: {
    displayName: 'perk';
  };
  attributes: {
    text: Attribute.String;
  };
}

export interface V2ProductPricingCards extends Schema.Component {
  collectionName: 'components_v2-product_pricing_cards';
  info: {
    displayName: 'Pricing Cards';
  };
  attributes: {
    savingsText: Attribute.RichText &
      Attribute.Required &
      Attribute.SetMinMaxLength<{
        minLength: 2;
      }> &
      Attribute.DefaultTo<'**Save 25%** with annually'>;
    productPlans: Attribute.Relation<
      'v2-product.pricing-cards',
      'oneToMany',
      'api::product-plan.product-plan'
    >;
  };
}

declare module '@strapi/types' {
  export module Shared {
    export interface Components {
      'common.action-button': CommonActionButton;
      'explainer-screen.continue-button': ExplainerScreenContinueButton;
      'explainer-screen.section': ExplainerScreenSection;
      'footer.columns': FooterColumns;
      'footer.link': FooterLink;
      'marketing-components.as-featured-in': MarketingComponentsAsFeaturedIn;
      'marketing-components.hero': MarketingComponentsHero;
      'marketing-components.marketing-page-section': MarketingComponentsMarketingPageSection;
      'marketing-components.section-tail': MarketingComponentsSectionTail;
      'marketing-components.stats-bar-stat': MarketingComponentsStatsBarStat;
      'metadata.general-page-metadata': MetadataGeneralPageMetadata;
      'onboarding-v5.action-button': OnboardingV5ActionButton;
      'onboarding-v5.carousel-item': OnboardingV5CarouselItem;
      'onboarding-v5.completion-step': OnboardingV5CompletionStep;
      'onboarding-v5.group-selector-step': OnboardingV5GroupSelectorStep;
      'onboarding-v5.onboarding-step': OnboardingV5OnboardingStep;
      'onboarding-v5.radio-option': OnboardingV5RadioOption;
      'onboarding-v5.skip-button': OnboardingV5SkipButton;
      'onboarding-v5.tag-selector-step': OnboardingV5TagSelectorStep;
      'onboarding-v5.user-selector-step': OnboardingV5UserSelectorStep;
      'onboarding-v5.verify-email-step': OnboardingV5VerifyEmailStep;
      'product.footer': ProductFooter;
      'product.hero': ProductHero;
      'product.other': ProductOther;
      'product.section': ProductSection;
      'tags.tags': TagsTags;
      'v2-product.action-button': V2ProductActionButton;
      'v2-product.basic-explainer': V2ProductBasicExplainer;
      'v2-product.closing-cta': V2ProductClosingCta;
      'v2-product.feature-highlight': V2ProductFeatureHighlight;
      'v2-product.feature-showcase-item': V2ProductFeatureShowcaseItem;
      'v2-product.feature-showcase': V2ProductFeatureShowcase;
      'v2-product.feature-table-header': V2ProductFeatureTableHeader;
      'v2-product.feature-table': V2ProductFeatureTable;
      'v2-product.hero': V2ProductHero;
      'v2-product.image-card': V2ProductImageCard;
      'v2-product.perk': V2ProductPerk;
      'v2-product.pricing-cards': V2ProductPricingCards;
    }
  }
}
