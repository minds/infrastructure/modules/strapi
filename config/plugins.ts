export default {
  //
  graphql: {
    config: {
      // https://www.apollographql.com/docs/apollo-server/api/apollo-server
      apolloServer: {
        introspection: true // default: introspection = NODE_ENV !== "production"
      },
    },
  },
};