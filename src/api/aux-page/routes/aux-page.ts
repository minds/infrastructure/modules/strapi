/**
 * aux-page router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::aux-page.aux-page');
