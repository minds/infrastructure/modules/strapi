/**
 * aux-page controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::aux-page.aux-page');
