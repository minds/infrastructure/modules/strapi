/**
 * aux-page service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::aux-page.aux-page');
