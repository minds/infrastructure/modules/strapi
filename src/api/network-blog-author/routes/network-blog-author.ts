/**
 * network-blog-author router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::network-blog-author.network-blog-author');
