/**
 * network-blog-author controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::network-blog-author.network-blog-author');
