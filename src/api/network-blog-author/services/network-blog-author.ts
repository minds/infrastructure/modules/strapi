/**
 * network-blog-author service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::network-blog-author.network-blog-author');
