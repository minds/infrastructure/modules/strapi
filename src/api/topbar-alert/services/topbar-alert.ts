/**
 * topbar-alert service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::topbar-alert.topbar-alert');
