/**
 * topbar-alert controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::topbar-alert.topbar-alert');
