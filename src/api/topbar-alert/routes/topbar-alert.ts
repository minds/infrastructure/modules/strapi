/**
 * topbar-alert router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::topbar-alert.topbar-alert');
