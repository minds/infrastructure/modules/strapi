/**
 * onboarding-v5-version controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::onboarding-v5-version.onboarding-v5-version');
