/**
 * onboarding-v5-version router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::onboarding-v5-version.onboarding-v5-version');
