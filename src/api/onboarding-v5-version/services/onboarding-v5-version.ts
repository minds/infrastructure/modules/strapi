/**
 * onboarding-v5-version service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::onboarding-v5-version.onboarding-v5-version');
