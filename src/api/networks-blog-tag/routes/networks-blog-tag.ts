/**
 * networks-blog-tag router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::networks-blog-tag.networks-blog-tag');
