/**
 * networks-blog-tag controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::networks-blog-tag.networks-blog-tag');
