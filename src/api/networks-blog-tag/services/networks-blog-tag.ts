/**
 * networks-blog-tag service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::networks-blog-tag.networks-blog-tag');
