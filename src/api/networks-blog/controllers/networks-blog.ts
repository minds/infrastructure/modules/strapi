/**
 * networks-blog controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::networks-blog.networks-blog');
