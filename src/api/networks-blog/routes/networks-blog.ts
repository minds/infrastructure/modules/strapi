/**
 * networks-blog router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::networks-blog.networks-blog');
