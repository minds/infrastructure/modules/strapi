/**
 * networks-blog service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::networks-blog.networks-blog');
