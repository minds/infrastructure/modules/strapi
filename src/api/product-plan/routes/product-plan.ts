/**
 * product-plan router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::product-plan.product-plan');
