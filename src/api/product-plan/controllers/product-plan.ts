/**
 * product-plan controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::product-plan.product-plan');
