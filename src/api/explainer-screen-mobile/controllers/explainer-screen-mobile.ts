/**
 * explainer-screen-mobile controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::explainer-screen-mobile.explainer-screen-mobile');
