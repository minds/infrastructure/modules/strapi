/**
 * explainer-screen-mobile router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::explainer-screen-mobile.explainer-screen-mobile');
