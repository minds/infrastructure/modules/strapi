/**
 * explainer-screen-mobile service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::explainer-screen-mobile.explainer-screen-mobile');
