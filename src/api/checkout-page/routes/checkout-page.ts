/**
 * checkout-page router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::checkout-page.checkout-page');
