/**
 * checkout-page service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::checkout-page.checkout-page');
