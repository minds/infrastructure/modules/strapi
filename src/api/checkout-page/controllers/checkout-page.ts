/**
 * checkout-page controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::checkout-page.checkout-page');
