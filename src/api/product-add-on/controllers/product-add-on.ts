/**
 * product-add-on controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::product-add-on.product-add-on');
