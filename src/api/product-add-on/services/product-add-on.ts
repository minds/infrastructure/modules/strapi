/**
 * product-add-on service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::product-add-on.product-add-on');
