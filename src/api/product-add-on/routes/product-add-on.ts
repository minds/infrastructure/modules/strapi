/**
 * product-add-on router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::product-add-on.product-add-on');
