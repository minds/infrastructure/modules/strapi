/**
 * feat-table-item router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::feat-table-item.feat-table-item');
