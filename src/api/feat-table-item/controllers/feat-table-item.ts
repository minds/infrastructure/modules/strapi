/**
 * feat-table-item controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::feat-table-item.feat-table-item');
