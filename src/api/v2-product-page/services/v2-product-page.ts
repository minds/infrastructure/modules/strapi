/**
 * v2-product-page service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::v2-product-page.v2-product-page');
