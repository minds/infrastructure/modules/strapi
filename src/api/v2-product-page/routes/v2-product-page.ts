/**
 * v2-product-page router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::v2-product-page.v2-product-page');
