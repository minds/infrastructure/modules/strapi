/**
 * v2-product-page controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::v2-product-page.v2-product-page');
