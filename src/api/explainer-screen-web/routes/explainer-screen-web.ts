/**
 * explainer-screen-web router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::explainer-screen-web.explainer-screen-web');
