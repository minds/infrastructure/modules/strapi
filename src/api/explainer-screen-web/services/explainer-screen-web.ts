/**
 * explainer-screen-web service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::explainer-screen-web.explainer-screen-web');
