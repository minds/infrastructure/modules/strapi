/**
 * explainer-screen-web controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::explainer-screen-web.explainer-screen-web');
