/**
 * upgrade-page controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::upgrade-page.upgrade-page');
