/**
 * upgrade-page service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::upgrade-page.upgrade-page');
