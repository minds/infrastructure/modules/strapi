/**
 * feat-table-section service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::feat-table-section.feat-table-section');
