/**
 * feat-table-section router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::feat-table-section.feat-table-section');
