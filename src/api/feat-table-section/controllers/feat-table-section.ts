/**
 * feat-table-section controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::feat-table-section.feat-table-section');
