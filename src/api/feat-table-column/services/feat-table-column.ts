/**
 * feat-table-column service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::feat-table-column.feat-table-column');
