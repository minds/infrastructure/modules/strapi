/**
 * feat-table-column controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::feat-table-column.feat-table-column');
