/**
 * feat-table-column router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::feat-table-column.feat-table-column');
