/**
 * value-prop-card controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::value-prop-card.value-prop-card');
