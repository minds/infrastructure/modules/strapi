/**
 * value-prop-card router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::value-prop-card.value-prop-card');
