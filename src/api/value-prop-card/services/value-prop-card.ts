/**
 * value-prop-card service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::value-prop-card.value-prop-card');
