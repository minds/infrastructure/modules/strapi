/**
 * twitter-sync-tweet-text controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::twitter-sync-tweet-text.twitter-sync-tweet-text');
