/**
 * twitter-sync-tweet-text service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::twitter-sync-tweet-text.twitter-sync-tweet-text');
