/**
 * twitter-sync-tweet-text router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::twitter-sync-tweet-text.twitter-sync-tweet-text');
